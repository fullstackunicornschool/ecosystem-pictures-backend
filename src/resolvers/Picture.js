import fs from "fs";
// import {exiftool} from 'exiftool-vendored';
import { v4 } from "uuid";
import Prisma from "@prisma/client";
import sizeOf from "image-size";
import statusMessage from "../tools/statusMessage.js";
import reBase62 from "../tools/rebase62.js";
import axios from "axios";

const prisma = new Prisma.PrismaClient();

export default class Picture {
	constructor() {
		this.extensions = {
			"image/jpeg": "jpg",
			"image/png": "png",
			"image/webp": "webp",
		};
		this.thumbs = ["TS", "TL", "MD", "HD"];
		this.thumbsServerUrl = process.env.MEDIA_TOOLS_URL;
	}

	rollbacKImageCreation = (pathSave, name) => {
		console.log("pathSave, name >", pathSave, name);

		// rollback original
		if (fs.existsSync(pathSave + name)) fs.unlinkSync(pathSave + name);

		// rollback each thumb
		this.thumbs.forEach((thumb) => {
			if (fs.existsSync(pathSave + `/${thumb}` + name))
				fs.unlinkSync(pathSave + `/${thumb}` + name);
		});
	};

	getType = (mimetype) => {
		let result = [this.extensions[mimetype]];
		if (result.includes("jpg")) result = ["jpg", "jpeg"];
		return result;
	};

	getFolders = () => {
		const basePath = process.env.__dirname + "/public";
		return {
			pathSave: `${basePath}`,
			pathDb: ``,
		};
	};

	// START OF ASYNC ##############################################

	fetch = async (request, response, next) => {
		try {
			const result = await prisma.picture.findMany({
				where: { author_id: request.user },
				orderBy: { path: "desc" },
			});
			console.log("findPictures result > ", result);
			response.status(200).send({ data: result });
		} catch (error) {
			console.log(error);
			return response.status(422).send(statusMessage[422]);
		}
	};

	new = async (request, response, next) => {
		let pathSave;
		let name;

		try {
			const file = request?.files?.file;

			// check if there is a file
			if (!file || !file?.data) return response.status(400).send(statusMessage[400]);

			const isMimetypeValid = file.mimetype in this.extensions;

			// check if mimetype is one of the list we accept in extensions
			if (!isMimetypeValid) return response.status(422).send(statusMessage[422]);

			// create filename from date and UUID V4 rebased to base62 (instead of 16)
			const fileName = `${reBase62(v4())}`;
			const fileExtension = this.extensions[file.mimetype];

			// get the folder path (and create them if it doesn't exist)
			const folders = this.getFolders();

			pathSave = folders.pathSave;

			// combine name and extensions with path to save the file
			name = `/${fileName}.${fileExtension}`;
			const fullName = pathSave + name;
			// save the file (if something goes wrong it goes into catch)
			await file.mv(fullName);

			// call imageMagix to create all thumbs
			const payload = { imgName: folders.pathDb + `/${this.thumbs[0]}` + name };
			const thumbs = await axios.post(this.thumbsServerUrl, payload);

			// fullName the file info on prisma database
			// default path is the 1st element of this.thumbs
			const newImage = await prisma.picture.create({
				data: {
					path: folders.pathDb + `/${this.thumbs[0]}` + name,
					author_id: request.user,
				},
			});

			//send newImage created as response
			response.status(201).send({
				message: statusMessage[201],
				data: newImage,
			});
		} catch (error) {
			this.rollbacKImageCreation(pathSave, name);
			console.log(error);
			return response.status(422).send(statusMessage[422]);
		}
	};
}
