import express from "express";
import fileUpload from "express-fileupload";
import routes from "./routes.js";

import path from "path";
import { fileURLToPath } from "url";

const { DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME } = process.env;

if (DB_USER && DB_PASSWORD && DB_HOST && DB_PORT && DB_NAME) {
	process.env.DATABASE_URL = `postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}?connection_limit=5&pool_timeout=2`;
}

const __filename = fileURLToPath(import.meta.url);
process.env.__dirname = path.dirname(__filename).replace("/src", "");

const app = express();
app.use(express.static("public"));
app.use(fileUpload());
app.use("/pictures", routes);
app.use("/alive", (req,res)=>{return res.status(200).send("ecosystem-media-backend is alive")})


export default app;
