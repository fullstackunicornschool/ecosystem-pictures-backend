const statusMessage = {
    '200': 'Success',
    '201': 'Created',
    '400': 'Bad Request',
    '401': 'Not Authorized',
    '404': 'Not Found',
    '409': 'Conflict',
    '422': 'Invalid Data'
};
export default statusMessage;
